from django.conf.urls import url
from django.urls import path, include
from .v1.urls import urlpatterns

urlpatterns = [
    url('v1', include(urlpatterns), name='articles_api_v1'),
]