from rest_framework import generics

from articles.api.v1.serializers import ArticleSerializer
from articles.models import Article


class ArticleListView(generics.ListAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
