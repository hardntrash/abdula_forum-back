from django.conf.urls import url
from django.urls import path

from articles.api.v1.views import ArticleListView

urlpatterns = [
    url('articles', ArticleListView.as_view(), name='get_all_articles_v1'),
]
