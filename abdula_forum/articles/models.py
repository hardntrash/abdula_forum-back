from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

# Create your models here.
from django.utils.safestring import mark_safe


class Article(models.Model):
    CATEGORY = (
        ('news', 'Новость'),
        ('blog', 'Блог'),
    )

    name = models.CharField(verbose_name='Заголовок', max_length=320)
    preview_image = models.ImageField(verbose_name='Превью: изображение')
    preview_video = models.FileField(verbose_name='Превью: видео', blank=True)
    title = models.TextField(verbose_name='Подзаголовок', blank=True)
    text = models.TextField(verbose_name='Основной текст')
    author = models.ForeignKey(User, verbose_name='Автор', on_delete=models.CASCADE)
    publish_date = models.DateTimeField(verbose_name='Дата публикации', default=timezone.now)
    is_main = models.BooleanField(verbose_name='Главная', default=False)
    is_important = models.BooleanField(verbose_name='Важная', default=False)
    category = models.CharField(verbose_name='Категория', max_length=4, choices=CATEGORY, default='news')

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ('publish_date',)
