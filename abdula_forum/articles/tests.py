from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import resolve
from django.http import HttpRequest
from rest_framework import status

from articles.api.v1.serializers import ArticleSerializer
from .models import Article

client = Client()


# Create your tests here.

class GetAllArticlesTest(TestCase):
    def setUp(self):
        redactor = User.objects.get_or_create(username='redach', first_name='Редактор', last_name='Сайта')[0]
        Article.objects.get_or_create(name='Тестовая статья 1', text='Описание статьи', author=redactor)
        Article.objects.get_or_create(name='Тестовая статья 2', text='Описание статьи', author=redactor)
        Article.objects.get_or_create(name='Тестовая статья 3', text='Описание статьи', author=redactor)

    def test_get_all_articles(self):
        articles = Article.objects.all()
        response = client.get('/api/v1/articles/')

        serializer = ArticleSerializer(articles, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
