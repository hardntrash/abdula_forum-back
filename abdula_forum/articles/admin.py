from django.contrib import admin
from django import forms

from .models import Article
from ckeditor.widgets import CKEditorWidget


# Register your models here.

class ArticleAdminForm(forms.ModelForm):
    text = forms.CharField(label='Основной текст', widget=CKEditorWidget())

    class Meta:
        model = Article
        fields = '__all__'


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm


admin.site.register(Article, ArticleAdmin)
